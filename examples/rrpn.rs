#![warn(clippy::pedantic)]
#![allow(clippy::unnecessary_wraps)]

use std::env;
use std::io::stdout;
use std::io::Write;
use std::path::Path;
use std::sync::Arc;
use std::sync::Mutex;

use num_traits::cast;

use rrpn::parsers;
use rrpn::stack::Error;
use rrpn::stack::Result;
use rrpn::stack::Stack;
use rrpn::stack::Word;
use rrpn::stack::Entry;
use rrpn::vocab;

/// All use of `linenoise` is introduced at the application level
fn history(_stack: &mut Stack) -> Result {
    let mut index = 0;
    loop {
        match linenoise::history_line(index) {
            None => break,
            Some(line) => println!("{}: {}", index, line),
        };
        index += 1;
    }
    Ok(())
}

/// No op for customisable repl words
fn noop(_stack: &mut Stack) -> Result {
    Ok(())
}

/// Provides the application's Read Evaluate Print Loop functionality
struct Repl {
    stack: Arc<Mutex<Stack>>,
}

/// Implementation of the REPL
impl Repl {
    /// Constructs the REPL environment
    fn new(stack: Stack) -> Self {
        Repl {
            stack: Arc::new(Mutex::new(stack)),
        }
    }

    /// Convenience method to evaluate a list of tokens and handle errors internally
    fn eval(&mut self, args: &[String]) {
        for arg in args {
            match self.stack.lock().unwrap().eval(arg) {
                Ok(_) => continue,
                Err(error) => println!("ERR: {}", error),
            }

            // We terminate on the first error
            break;
        }
    }

    /// Provides the Repl loop
    fn shell(&mut self) {
        let weak_ptr = Arc::downgrade(&self.stack);

        linenoise::set_callback_with_fn(move |input| {
            if let Some(stack) = weak_ptr.upgrade() {
                let mut lock = stack.lock().unwrap();
                lock.matches(input)
            } else {
                Vec::new()
            }
        });

        loop {
            match linenoise::input(if self.stack.lock().unwrap().parsing() {
                ">> "
            } else {
                "> "
            }) {
                None => break,
                Some(input) => {
                    if input != "history" {
                        linenoise::history_add(&input);
                    }
                    self.exec("repl-ante");
                    match self.stack.lock().unwrap().command(&input) {
                        Ok(_) | Err(Error::Comment) => (),
                        Err(error) => println!("ERR: {}", error),
                    }
                    self.exec("repl-post");
                    if !input.is_empty() && !self.stack.lock().unwrap().parsing() {
                        self.exec("repl-debug");
                    }
                    let _ignore = stdout().flush();
                }
            }
        }
    }

    fn exec(&mut self, token: &str) {
        if !self.stack.lock().unwrap().parsing() {
            let _ignore = self.stack.lock().unwrap().eval(token);
        }
    }
}

/// Outputs the top of stack if it exists and cleans linenoise
impl Drop for Repl {
    fn drop(&mut self) {
        match self.stack.lock().unwrap().last() {
            Some(value) => println!("{}", value),
            None => (),
        }

        linenoise::reset();
    }
}

/// Constructs the REPL instance and evaluates command line arguments
fn run() {
    let mut stack: Stack = Stack::new();

    vocab::teach(&mut stack);
    parsers::teach(&mut stack);

    vocab::Vocab::new(&mut stack, "system").add("history", Word::Function(history));

    vocab::Vocab::new(&mut stack, "repl")
        .add("repl-ante", Word::Function(noop))
        .add("repl-post", Word::Function(noop))
        .add("repl-debug", Word::Token("dump".to_string()));

    let dim = linenoise::dimensions();
    vocab::Vocab::new(&mut stack, "terminal")
        .add( "t-width", Word::Value(Entry::Double(cast::<i32, f64>(dim.0).unwrap())),)
        .add( "t-height", Word::Value(Entry::Double(cast::<i32, f64>(dim.1).unwrap())),);

    let mut repl = Repl::new(stack);

    let args: Vec<String> = env::args().collect();
    let mut index = 1;

    // Provides shebang support - first arg can be a file
    if args.len() >= 2 {
        let name = args.get(1).unwrap();
        if Path::new(&name).exists() {
            repl.eval(&[String::from("include"), name.to_string()]);
            index += 1;
        }
    }

    // If we have additional arguments, eval and exit - otherwise enter shell
    if args.len() > index {
        repl.eval(args.split_at(index).1);
    } else {
        repl.shell();
    }
}

/// The main function
fn main() {
    run();
}
