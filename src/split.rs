//! String tokeniser which honours quotes

/// Structure just holds reference to remainder of str
pub struct Split<'s> {
    src: &'s str,
}

/// Implementation of Split
impl<'s> Split<'s> {
    /// Constructor of Split
    pub fn new(src: &'s str) -> Self {
        Self { src }
    }
}

/// Iterator implementation for split
impl<'s> Iterator for Split<'s> {
    /// Currently returns str slices, but should really become String to support escapes correctly
    type Item = &'s str;

    /// Returns the next token, leaving remainder in self.src
    fn next(&mut self) -> Option<Self::Item> {
        self.src = self.src.trim_start();
        let mut indices = self.src.char_indices();
        if let Some((start, first)) = indices.next() {
            let quoted = first == '\"' || first == '\'';
            let mut escaped = false;

            for (index, current) in indices {
                if escaped {
                    escaped = false;
                } else if quoted && current == '\\' {
                    escaped = true;
                } else if quoted && current == first {
                    return self.handle(&self.src[start + 1..index], index + 1);
                } else if !quoted && current == ' ' {
                    return self.handle(&self.src[start..index], index + 1);
                }
            }

            let offset = if quoted && !escaped { 1 } else { 0 };
            return self.handle(&self.src[start + offset..], self.src.len());
        }
        None
    }
}

/// Trait which handles token and maintains remainder
trait Handle<'s>: Iterator {
    fn handle(&mut self, token: &'s str, offset: usize) -> Option<Self::Item>;
}

/// Implementation of handle trait
impl<'s> Handle<'s> for Split<'s> {
    fn handle(&mut self, token: &'s str, offset: usize) -> Option<Self::Item> {
        self.src = &self.src[offset..];
        Some(token)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty() {
        let mut split = Split::new("");
        assert_eq!(None, split.next());
    }

    #[test]
    fn spaces() {
        let mut split = Split::new("    ");
        assert_eq!(None, split.next());
    }

    #[test]
    fn leading() {
        let mut split = Split::new("  hello");
        assert_eq!(Some("hello"), split.next());
        assert_eq!(None, split.next());
    }

    #[test]
    fn tokens() {
        let mut split = Split::new("this    is    a    test");
        assert_eq!(Some("this"), split.next());
        assert_eq!(Some("is"), split.next());
        assert_eq!(Some("a"), split.next());
        assert_eq!(Some("test"), split.next());
        assert_eq!(None, split.next());
    }

    #[test]
    fn quotes() {
        let mut split = Split::new("'to be or not to be' ... \" that is 'the'   ?\"    ");
        assert_eq!(Some("to be or not to be"), split.next());
        assert_eq!(Some("..."), split.next());
        assert_eq!(Some(" that is 'the'   ?"), split.next());
        assert_eq!(None, split.next());
    }
}
