//! Rust implementation of a Reverse Polish Notation calculator.

#![warn(clippy::pedantic)]
#![allow(clippy::unnecessary_wraps)]

use std::borrow::Borrow;
use std::cmp::Ordering;
use std::collections::BTreeMap;
use std::fmt;
use std::fmt::Formatter;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::rc::Rc;
use std::str::FromStr;

use num_rational::Rational64;
use num_traits::cast;
use num_complex::Complex64;

use crate::lexicon::Lexicon;
use crate::split::Split;
use crate::io::Io;
use crate::io::Stdio;

/// Types of errors the stack words can encounter
pub enum Error {
    Underflow,
    ParserError,
    Comment,
    Conversion,
    InvalidToken(String),
    CompilerWord(String),
    FileNotFound(String),
    Leave(String),
    IndexNotInScope(String),
    InvalidType(String),
    OutOfBounds(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Error::Underflow => write!(f, "Underflow"),
            Error::ParserError => write!(f, "ParserError"),
            Error::Comment => write!(f, "Comment"),
            Error::Conversion => write!(f, "ConversionErrror"),
            Error::InvalidToken(token) => write!(f, "InvalidToken: {}", token),
            Error::CompilerWord(token) => write!(f, "CompilerWord: {}", token),
            Error::FileNotFound(token) => write!(f, "FileNotFound: {}", token),
            Error::Leave(_) => write!(f, "Leave"),
            Error::IndexNotInScope(token) => write!(f, "IndexNotInScope: {}", token),
            Error::InvalidType(token) => write!(f, "InvalidType: {}", token),
            Error::OutOfBounds(token) => write!(f, "OutOfBounds: {}", token),
        }
    }
}

pub struct Vector {
    pub data: Vec<Entry>,
}

impl Vector {
    #[must_use]
    pub fn new() -> Self {
        Vector { data: Vec::new() }
    }
}

impl Default for Vector {
    #[must_use]
    fn default() -> Self {
        Vector::new()
    }
}

impl PartialEq for Vector {
    fn eq(&self, _other: &Self) -> bool {
        false
    }
}

impl Clone for Vector {
    fn clone(&self) -> Self {
        Vector {
            data: self.data.clone(),
        }
    }
}

impl fmt::Display for Vector {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_list().entries(self.data.iter()).finish()
    }
}

impl fmt::Debug for Vector {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_list().entries(self.data.iter()).finish()
    }
}

#[derive(PartialEq, Clone)]
pub enum Entry {
    Double(f64),
    Bool(bool),
    Rational(Rational64),
    String(String),
    Vector(Vector),
    Complex(Complex64),
}

impl PartialOrd for Entry {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Entry::Double(v1), Entry::Double(v2)) => v1.partial_cmp(v2),
            (Entry::Bool(v1), Entry::Bool(v2)) => v1.partial_cmp(v2),
            (Entry::Rational(v1), Entry::Rational(v2)) => v1.partial_cmp(v2),
            (Entry::String(v1), Entry::String(v2)) => v1.partial_cmp(v2),
            _ => None,
        }
    }
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use std::fmt::Display;
        let rnd = 10_000_000_000_000.0_f64;
        match self {
            Entry::Double(value) => Display::fmt(&((value * rnd).round() / rnd), f),
            Entry::Bool(value) => Display::fmt(&value, f),
            Entry::Rational(value) => Display::fmt(&value, f),
            Entry::String(value) => Display::fmt(&value, f),
            Entry::Vector(value) => Display::fmt(&value, f),
            Entry::Complex(value) => Display::fmt(&Complex64::new((value.re * rnd).round() / rnd, (value.im * rnd).round() / rnd ), f),
        }
    }
}

impl fmt::Debug for Entry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use std::fmt::Debug;
        let rnd = 10_000_000_000_000.0_f64;
        match self {
            Entry::Double(value) => Debug::fmt(&((value * rnd).round() / rnd), f),
            Entry::Bool(value) => Debug::fmt(&value, f),
            Entry::Rational(value) => write!(f, "{}/{}", value.numer(), value.denom()),
            Entry::String(value) => Debug::fmt(&value, f),
            Entry::Vector(_value) => write!(f, "[vec...]"),
            Entry::Complex(value) => write!(f, "{}+{}i", (value.re * rnd).round() / rnd, (value.im * rnd).round() / rnd ),
        }
    }
}

fn to_f64(rat: &Rational64) -> f64 {
    cast::<i64, f64>(*rat.numer()).unwrap() / cast::<i64, f64>(*rat.denom()).unwrap()
}

fn to_usize(value: f64) -> Option<usize> {
    cast::<f64, usize>(value)
}

fn from_base( token: &str, prefix: &str, base: u32 ) -> Option<f64> {
    if token.starts_with( prefix ) {
        let without_prefix = token.trim_start_matches( prefix );
        cast::< i64, f64 >( i64::from_str_radix( without_prefix, base ).unwrap( ) )
    } else {
        None
    }
}

impl Entry {
    #[must_use]
    pub fn eval(token: &str) -> Self {
        match token.parse::<f64>() {
            Ok(value) => Entry::Double(value),
            _ => match token.parse::<Rational64>() {
                Ok(value) => Entry::Rational(value),
                _ => match Complex64::from_str(token) {
                    Ok(value) => Entry::Complex(value),
                    _ => Entry::String(token.to_string()),
                },
            },
        }
    }

    #[must_use]
    pub fn as_usize(&self) -> Option<usize> {
        match self {
            Entry::Double(value) => to_usize(*value),
            _ => None,
        }
    }

    #[must_use]
    pub fn as_f64(&self) -> Option<f64> {
        match self {
            Entry::Double(value) => Some(*value),
            Entry::Bool(value) => Some(if *value { 1.0 } else { 0.0 }),
            Entry::Rational(value) => Some(to_f64(value)),
            Entry::String(_) | Entry::Vector(_) | Entry::Complex(_) => None,
        }
    }

    #[must_use]
    pub fn as_mut_f64(&mut self) -> Option<&mut f64> {
        match self {
            Entry::Double(value) => Some(value),
            Entry::Bool(_) | Entry::Rational(_) | Entry::String(_) | Entry::Vector(_) | Entry::Complex(_) => None,
        }
    }

    #[must_use]
    pub fn as_bool(&self) -> Option<bool> {
        match self {
            Entry::Double(value) => Some(*value != 0.0),
            Entry::Bool(value) => Some(*value),
            Entry::Rational(value) => Some(to_f64(value) != 0.0),
            Entry::String(_) | Entry::Vector(_) | Entry::Complex(_) => None,
        }
    }

    #[must_use]
    pub fn as_mut_bool(&mut self) -> Option<&mut bool> {
        match self {
            Entry::Bool(value) => Some(value),
            Entry::Double(_) | Entry::Rational(_) | Entry::String(_) | Entry::Vector(_) | Entry::Complex(_) => None,
        }
    }

    #[must_use]
    pub fn as_r64(&self) -> Option<Rational64> {
        match self {
            Entry::Double(value) => {
                if *value == value.trunc() {
                    Some(Rational64::new(*value as i64, 1))
                } else {
                    None
                }
            }
            Entry::Bool(value) => Some(if *value {
                Rational64::new(1, 1)
            } else {
                Rational64::new(0, 1)
            }),
            Entry::Rational(value) => Some(*value),
            Entry::String(_) | Entry::Vector(_) | Entry::Complex(_) => None,
        }
    }

    #[must_use]
    pub fn as_mut_r64(&mut self) -> Option<&mut Rational64> {
        match self {
            Entry::Rational(value) => Some(value),
            Entry::Bool(_) | Entry::Double(_) | Entry::String(_) | Entry::Vector(_) | Entry::Complex(_) => None,
        }
    }

    #[must_use]
    pub fn as_c64(&self) -> Option<Complex64> {
        match self {
            Entry::Double(value) => {
                if *value == value.trunc() {
                    Some(Complex64::new(*value as f64, 0.0))
                } else {
                    None
                }
            }
            Entry::Bool(value) => Some(if *value {
                Complex64::new(1.0, 0.0)
            } else {
                Complex64::new(0.0, 0.0)
            }),
            Entry::Complex(value) => Some(*value),
            Entry::String(_) | Entry::Vector(_) | Entry::Rational(_) => None,
        }
    }

    #[must_use]
    pub fn as_mut_c64(&mut self) -> Option<&mut Complex64> {
        match self {
            Entry::Complex(value) => Some(value),
            Entry::Bool(_) | Entry::Double(_) | Entry::String(_) | Entry::Vector(_) | Entry::Rational(_) => None,
        }
    }
}

/// Defines the result type of the stack operators
pub type Result = ::std::result::Result<(), Error>;

pub type Modifier = fn(&mut Stack) -> Result;

/// Enums for stack word types
pub enum Word {
    /// Holds a value
    Value(Entry),

    /// Named word
    Named(String, Modifier),

    /// Native rust fn for normal evaluation words
    Function(Modifier),

    /// Parser words are used to parse the token input stream
    Parser(Modifier),

    /// Special case for a user defined word
    Define(Rc<dyn FullParser>),

    /// Fallback to hold a token if necessary
    Token(String),
}

impl fmt::Debug for Word {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Word::Value(value) => std::fmt::Debug::fmt(&value, f),
            Word::Token(value) => std::fmt::Debug::fmt(&value, f),
            Word::Define(value) => std::fmt::Debug::fmt(&value, f),
            Word::Named(name, _) => write!(f, "{}", name),
            Word::Function(_) => write!(f, "function"),
            Word::Parser(_) => write!(f, "parser"),
        }
    }
}

/// Implementation of Word enum
impl Word {
    /// Evaluates a word on the given stack
    /// # Errors
    /// TODO: Check what these are
    pub fn eval(&self, stack: &mut Stack) -> Result {
        match self {
            Word::Value(value) => {
                stack.push(value.clone());
                Ok(())
            }
            Word::Function(func) | Word::Parser(func) | Word::Named(_, func) => func(stack),
            Word::Define(define) => {
                let copy = define.clone();
                copy.eval(stack)
            }
            Word::Token(token) => stack.eval(token),
        }
    }
}

/// A parsers 'parse' method returns a state of done, next or the token requires evaluating
pub enum State {
    Done,
    Next,
    Eval,
}

/// Provides a trait for parsing words to implement
pub trait Parser {
    /// The active parser will receive all tokens through this method - return true when complete
    fn parse(&mut self, stack: &Stack, token: &str) -> State;

    /// If the parse method can return Parse, this method should be implemented to handled the generated parser
    fn include(&mut self, _other: Rc<dyn FullParser>) {
        panic!("Inclusion of parsers not implemented");
    }

    /// Returns the name if this parser should be stored in the dictionary
    fn word(&self) -> Option<String> {
        None
    }

    fn entry(&self) -> Option<Entry> {
        None
    }

    /// Evaluate the parsed tokens
    /// # Errors
    /// Will return the `Error` of the last word executed
    fn eval(&self, stack: &mut Stack) -> Result;
}

pub trait FullParser: Parser + std::fmt::Debug {}

/// A stack contains its data and a dictionary.
pub struct Stack {
    /// This is the data stack
    pub data: Vec<Entry>,

    /// This provides the word definitions
    dict: BTreeMap<String, Rc<Word>>,

    /// These are the 'open' vocabs - next word added belongs to last
    vocab: Vec<String>,

    /// This provides the vocabularies
    pub(crate) lexicon: Lexicon,

    /// This holds the active parsers
    pub(crate) parser: Vec<Rc<dyn FullParser>>,

    /// This holds the loop indexes
    pub(crate) indexes: Vec<Entry>,

    /// This holds the local variables
    pub(crate) locals: Vec< BTreeMap<String, Entry> >,

    /// This holds the io struct
    pub io: Box< dyn Io >,
}

impl Default for Stack {
    fn default() -> Self {
        Self::new()
    }
}

/// Implementation of a stack
impl Stack {
    /// Creates a new stack instance
    #[must_use]
    pub fn new() -> Self {
        Stack {
            data: Vec::new(),
            dict: BTreeMap::new(),
            vocab: Vec::new(),
            lexicon: Lexicon::new(),
            parser: Vec::new(),
            indexes: Vec::new(),
            locals: Vec::new(),
            io: Box::new( Stdio::new() ),
        }
    }

    /// Opens a vocab for new word definitions
    pub(crate) fn vocab_open(&mut self, vocab: &str) {
        self.vocab.push(vocab.to_string());
    }

    /// Returns the current vocab, defaulting to 'user' if none are open
    fn vocab_get(&self) -> String {
        match self.vocab.last() {
            Some(vocab) => vocab.to_string(),
            None => "user".to_string(),
        }
    }

    /// Closes the current vocab
    /// # Panics
    /// Panics if not paired with `vocab_open` call
    pub(crate) fn vocab_close(&mut self) {
        match self.vocab.pop() {
            Some(_) => (),
            None => panic!("ERR: Unbalanced vocab - fix your code"),
        };
    }

    /// Opens a local scope
    pub(crate) fn local_open(&mut self) {
        self.locals.push( BTreeMap::new( ) );
    }

    /// Close a local scope
    pub(crate) fn local_close(&mut self) {
        match self.locals.pop() {
            Some(_) => (),
            None => panic!("ERR: Unbalanced scope close"),
        };
    }

    /// Adds a new word
    pub fn add(&mut self, name: &str, word: Word) {
        let word = match word {
            Word::Function(func) => Word::Named(name.to_string(), func),
            _ => word,
        };
        let vocab = self.vocab_get();
        self.lexicon.define(&vocab, name);
        self.dict.insert(name.to_string(), Rc::new(word));
    }

    /// Obtain the word definition if it exists
    #[must_use]
    pub fn get(&self, name: &str) -> Option<&Rc<Word>> {
        self.dict.get(name)
    }

    /// Returns true if the word exists
    #[must_use]
    pub fn exists(&self, name: &str) -> bool {
        self.dict.contains_key(name)
    }

    /// Returns true if the word exists and is a parser
    /// # Panics
    /// Clippy says it might, I say it won't
    #[must_use]
    pub fn is_parser(&self, name: &str) -> bool {
        self.exists(name)
            && matches!(
                self.dict.get(name).unwrap().clone().borrow(),
                Word::Parser(_)
            )
    }

    /// Returns true if the stack is in parsing mode
    #[must_use]
    pub fn parsing(&self) -> bool {
        !self.parser.is_empty()
    }

    /// Deletes a previously added word
    pub fn forget(&mut self, word: &str) {
        self.lexicon.forget(word);
        self.dict.remove(word);
    }

    /// Evaluates a token (being a string which is either a word or a value)
    /// # Errors
    /// Can fail for any reason
    pub fn eval(&mut self, token: &str) -> Result {
        if self.parser.is_empty() && token.starts_with('#') {
            return Err(Error::Comment);
        }
        if self.parser.is_empty() {
            self.exec(token)
        } else {
            self.parse(token)
        }
    }

    /// Evaluates a command by first spliting by whitespace and evaluating each token
    /// # Errors
    /// Can fail for any reason
    pub fn command(&mut self, command: &str) -> Result {
        for token in Split::new(command) {
            self.eval(token)?;
        }
        Ok(())
    }

    /// Evaluates the contents of a file
    /// # Panics
    /// .. shouldn't?
    /// # Errors
    /// Can fail for any reason
    pub fn include(&mut self, filename: &str) -> Result {
        let file = File::open(filename);
        match file {
            Ok(fd) => {
                let reader = BufReader::new(fd);
                for line in reader.lines() {
                    match self.command(&line.unwrap()) {
                        Ok(_) | Err(Error::Comment) => (),
                        Err(error) => {
                            return Err(error);
                        }
                    }
                }
            }
            Err(_) => {
                return Err(Error::FileNotFound(String::from(filename)));
            }
        };

        Ok(())
    }

    /// Executes an individual token
    /// # Errors
    /// Can fail for any reason
    fn exec(&mut self, token: &str) -> Result {
        let word = self.dict.get(token);
        match word {
            Some(word) => Rc::clone(word).eval(self)?,
            _ => match token.parse::<f64>() {
                Ok(value) => {
                    self.push(Entry::Double(value));
                }
                _ => match token.parse::<Rational64>() {
                    Ok(value) => {
                        self.push(Entry::Rational(value));
                    }
                    _ => match token.parse::<Complex64>() {
                        Ok(value) => {
                            self.push(Entry::Complex(value));
                        }
                        _ => match from_base( token, "0x", 16 ) {
                            Some( value ) => {
                                self.push( Entry::Double( value ) );
                            }
                            _ => match from_base( token, "0b", 2 ) {
                                Some( value ) => {
                                    self.push( Entry::Double( value ) );
                                }
                                _ => match from_base( token, "0o", 8 ) {
                                    Some( value ) => {
                                        self.push( Entry::Double( value ) );
                                    }
                                    _ => {
                                        return Err(Error::InvalidToken(token.to_string()));
                                    }
                                },
                            },
                        },
                    },
                },
            },
        };
        Ok(())
    }

    /// Handles the parsers 'parse' methods and returned State
    /// # Errors
    /// Can fail for any reason
    fn parse(&mut self, token: &str) -> Result {
        let mut parser = self.parser.pop().ok_or(Error::ParserError)?;
        match Rc::get_mut(&mut parser).unwrap().parse(self, token) {
            State::Done => {
                if self.parser.is_empty() {
                    match parser.word() {
                        Some(name) => self.add(&name, Word::Define(parser)),
                        None => parser.eval(self)?,
                    }
                } else {
                    let active = self.parser.last_mut().ok_or(Error::ParserError)?;
                    Rc::get_mut(active).unwrap().include(parser);
                }
            }
            State::Next => {
                self.parser.push(parser);
            }
            State::Eval => {
                self.parser.push(parser);
                self.exec(token)?;
            }
        };
        Ok(())
    }

    /// Obtains the number of items on the data stack
    #[must_use]
    pub fn depth(&self) -> usize {
        self.data.len()
    }

    /// Pops the top of stack
    pub fn pop(&mut self) -> Option<Entry> {
        self.data.pop()
    }

    /// Pushes the provided item to the top of the stack
    pub fn push(&mut self, item: Entry) {
        self.data.push(item);
    }

    /// Returns a mutable reference to the top of the stack
    pub fn last(&mut self) -> Option<&mut Entry> {
        self.data.last_mut()
    }

    /// Returns a reference to the item at the specified index (where 0 is top of stack)
    #[must_use]
    pub fn pick(&self, index: usize) -> Option<&Entry> {
        let depth = self.depth();
        self.data.get(depth - index - 1)
    }

    /// Moves the item at the specified index to the top of the stack
    pub fn roll(&mut self, index: usize) {
        let pos = self.depth() - index - 1;
        let value = self.data.remove(pos);
        self.data.push(value);
    }

    /// Clears the stack
    pub fn clear(&mut self) {
        self.data.clear();
    }

    /// Obtains a list of words which start with 'input'
    pub fn matches(&mut self, input: &str) -> Vec<String> {
        let mut result: Vec<String> = Vec::new();
        let range = self.dict.range(input.to_string()..);
        for item in range {
            if item.0.starts_with(input) {
                result.push(item.0.to_string());
            }
        }
        result
    }
}
