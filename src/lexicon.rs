//! Provides vocabularies and word grouping functionality

use std::collections::btree_map::Keys;
use std::collections::BTreeMap;
use std::collections::BTreeSet;

/// Provides an index of vocabularies and the words they contain
pub struct Lexicon {
    /// Key is the vocab, value is the set of words in that vocab
    map: BTreeMap<String, BTreeSet<String>>,

    /// Key is the word, value is the vocab it belongs to
    rmap: BTreeMap<String, String>,
}

impl Default for Lexicon {
    fn default() -> Self {
        Self::new()
    }
}

/// Provides implemenation for lexicon
impl Lexicon {
    /// Provides the constructor for the lexicon
    pub fn new() -> Self {
        Lexicon {
            map: BTreeMap::new(),
            rmap: BTreeMap::new(),
        }
    }

    /// Provides the list of vocabularies defined
    pub fn vocabs(&self) -> Keys<'_, String, BTreeSet<String>> {
        self.map.keys()
    }

    /// Provides the list of words in the specified vocabulary
    pub fn words(&self, vocab: &String) -> Option<&BTreeSet<String>> {
        self.map.get(vocab)
    }

    /// Removes a word from the lexicon if it exists
    pub fn forget(&mut self, word: &str) {
        if self.rmap.contains_key(word) {
            let vocab = &self.rmap[word];
            let set = match self.map.get_mut(vocab) {
                Some(entry) => entry,
                None => panic!("ERR: Adding to non-existent vocab - how?"),
            };
            set.remove(word);
            self.rmap.remove(word);
        }
    }

    /// Defines a word within the requested vocabulary, removing it if previously defined
    pub fn define(&mut self, vocab: &String, word: &str) {
        if !self.map.contains_key(vocab) {
            self.map.insert(vocab.to_string(), BTreeSet::new());
        }
        self.forget(word);
        let set = match self.map.get_mut(vocab) {
            Some(entry) => entry,
            None => panic!("ERR: Adding to non-existent vocab - how?"),
        };
        set.insert(word.to_string());
        self.rmap.insert(word.to_string(), vocab.to_string());
    }
}
