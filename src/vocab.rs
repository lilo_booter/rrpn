//! Provides a collection of core vocabularies

#![warn(clippy::pedantic)]
#![allow(clippy::unnecessary_wraps)]

use num_traits::cast::cast;
use num_traits::FloatConst;

use std::ops::Neg;
use std::thread;
use std::time;
use std::time::SystemTime;
use std::time::UNIX_EPOCH;
use std::include_str;

use crate::stack::Entry;
use crate::stack::Error;
use crate::stack::Result;
use crate::stack::Stack;
use crate::stack::Word;

use crate::macros::op_bool;
use crate::macros::op_const;
use crate::macros::op_f64_infix;
use crate::macros::op_infix;
use crate::macros::op_logic;
use crate::macros::op_pair;
use crate::macros::op_prefix;
use crate::macros::op_r64_infix;
use crate::macros::op_c64_infix;
use crate::macros::op_c64_pair;
use crate::macros::op_c64_prefix;

/// Vocab guard
pub struct Vocab<'a> {
    pub stack: &'a mut Stack,
}

/// Implementation of vocab guard
impl<'a> Vocab<'a> {
    /// Constructor for vocab guard
    pub fn new(stack: &'a mut Stack, name: &str) -> Self {
        stack.vocab_open(name);
        Self { stack }
    }

    pub fn add(&mut self, name: &str, word: Word) -> &mut Self {
        self.stack.add(name, word);
        self
    }
}

impl<'a> Drop for Vocab<'a> {
    /// Destructor for vocab guard
    fn drop(&mut self) {
        (*self.stack).vocab_close();
    }
}

/// Pick nth item and duplicate at top ( nN ... n0 i -- nN ... n0 ni )
fn pick(stack: &mut Stack) -> Result {
    let index = stack
        .pop()
        .ok_or(Error::Underflow)?
        .as_f64()
        .ok_or(Error::Conversion)?;
    if index < 0.0 {
        return Err(Error::Underflow);
    }
    let index: usize = cast(index).unwrap();
    let depth = stack.depth();
    if index >= depth {
        return Err(Error::Underflow);
    }
    stack.push(stack.pick(index).ok_or(Error::Underflow)?.clone());
    Ok(())
}

/// Moves nth item on stack to top
fn roll(stack: &mut Stack) -> Result {
    let index = stack
        .pop()
        .ok_or(Error::Underflow)?
        .as_f64()
        .ok_or(Error::Conversion)?;
    if index < 0.0 {
        return Err(Error::Underflow);
    }
    let index: usize = cast(index).unwrap();
    let depth = stack.depth();
    if index >= depth {
        return Err(Error::Underflow);
    }
    stack.roll(index);
    Ok(())
}

/// Duplicate if non-zero
fn dup_nonzero(stack: &mut Stack) -> Result {
    let tos = stack.pick(0).ok_or(Error::Underflow)?;
    if tos.as_bool().ok_or(Error::Conversion)? {
        let copy = tos.clone();
        stack.push(copy);
    }
    Ok(())
}

/// Drops the item at the top of the stack ( a -- )
fn drop(stack: &mut Stack) -> Result {
    stack.pop().ok_or(Error::Underflow)?;
    Ok(())
}

/// Pushes current stack depth to stack
fn depth(stack: &mut Stack) -> Result {
    stack.push(Entry::Double(cast(stack.depth()).unwrap()));
    Ok(())
}

fn op_true(stack: &mut Stack) -> Result {
    stack.push(Entry::Bool(true));
    Ok(())
}

fn op_false(stack: &mut Stack) -> Result {
    stack.push(Entry::Bool(false));
    Ok(())
}

/// Logical not
fn not(stack: &mut Stack) -> Result {
    let tos: &mut bool = stack
        .last()
        .ok_or(Error::Underflow)?
        .as_mut_bool()
        .ok_or(Error::Conversion)?;
    *tos = !*tos;
    Ok(())
}

/// Clears the data stack ( nN ... n0 -- )
fn clear(stack: &mut Stack) -> Result {
    stack.clear();
    Ok(())
}

/// Prints the item at the top of the stack ( n -- )
fn dot(stack: &mut Stack) -> Result {
    let result = stack.pop().ok_or(Error::Underflow)?;
    stack.io.println(&format!("{}",result));
    Ok(())
}

/// Prints the item at the top of the stack without linefeed ( n -- )
fn comma(stack: &mut Stack) -> Result {
    let result = stack.pop().ok_or(Error::Underflow)?;
    stack.io.print(&format!("{}",result));
    Ok(())
}

fn dotr(stack: &mut Stack) -> Result {
    let width = stack
        .pop()
        .ok_or(Error::Underflow)?
        .as_f64()
        .ok_or(Error::Conversion)?;
    let width: usize = cast(width).unwrap();
    let result = stack.pop().ok_or(Error::Underflow)?;
    stack.io.print(&format!("{:width$}", result, width = width));
    Ok(())
}

fn dotl(stack: &mut Stack) -> Result {
    let width = stack
        .pop()
        .ok_or(Error::Underflow)?
        .as_f64()
        .ok_or(Error::Conversion)?;
    let width: usize = cast(width).unwrap();
    let result = stack.pop().ok_or(Error::Underflow)?;
    stack.io.print(&format!("{:<width$}", result, width = width));
    Ok(())
}

fn dotc(stack: &mut Stack) -> Result {
    let width = stack
        .pop()
        .ok_or(Error::Underflow)?
        .as_f64()
        .ok_or(Error::Conversion)?;
    let width: usize = cast(width).unwrap();
    let result = stack.pop().ok_or(Error::Underflow)?;
    stack.io.print(&format!("{:^width$}", result, width = width));
    Ok(())
}

/// Lists the contents of the stack
fn dump(stack: &mut Stack) -> Result {
    if !stack.data.is_empty() {
        stack.io.print("# ");
        for entry in &stack.data {
            stack.io.print(&format!("{:#?} ", entry));
        }
        stack.io.println("");
    }
    Ok(())
}

fn emit(stack: &mut Stack) -> Result {
    let tos = stack
        .pop()
        .ok_or(Error::Underflow)?
        .as_f64()
        .ok_or(Error::Conversion)?;
    let tos = cast::<f64, u32>(tos);
    stack.io.print(&format!( "{}", std::char::from_u32(tos.ok_or(Error::Underflow)?).unwrap()));
    Ok(())
}

fn flush(stack: &mut Stack) -> Result {
    stack.io.flush();
    Ok(())
}

/// Print the contents of the stack's lexicon
fn help(stack: &mut Stack) -> Result {
    let mut width = 0;

    let max_width: usize = match stack.eval("t-width") {
        Ok(()) => cast::<f64, usize>(
            stack
                .pop()
                .ok_or(Error::Underflow)?
                .as_f64()
                .ok_or(Error::Conversion)?,
        )
        .unwrap(),
        _ => 80_usize,
    };

    for key in stack.lexicon.vocabs() {
        if !stack.lexicon.words(key).unwrap().is_empty() {
            width = width.max(key.len());
        }
    }

    for key in stack.lexicon.vocabs() {
        if !stack.lexicon.words(key).unwrap().is_empty() {
            let mut used = 0;
            for word in stack.lexicon.words(key).unwrap() {
                if used != 0 && used + word.len() >= max_width {
                    stack.io.println("");
                    used = 0;
                }
                if used == 0 {
                    stack.io.print(&format!("{:1$}: ", key, width + 1));
                    stack.io.print(&format!("{} ", word));
                    used += width + word.len() + 4;
                } else {
                    stack.io.print(&format!("{} ", word));
                    used += word.len() + 1;
                }
            }
            stack.io.println("");
        }
    }
    Ok(())
}

fn sleep(stack: &mut Stack) -> Result {
    let tos = stack
        .pop()
        .ok_or(Error::Underflow)?
        .as_f64()
        .ok_or(Error::Conversion)?;
    let ms: u64 = cast(tos * 1000.0).unwrap();
    thread::sleep(time::Duration::from_millis(ms));
    Ok(())
}

fn time_since_epoch() -> f64 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs_f64()
}

fn now(stack: &mut Stack) -> Result {
    stack.push(Entry::Double(time_since_epoch()));
    Ok(())
}

fn elapsed(stack: &mut Stack) -> Result {
    let tos = stack
        .pop()
        .ok_or(Error::Underflow)?
        .as_f64()
        .ok_or(Error::Conversion)?;
    stack.push(Entry::Double(time_since_epoch() - tos));
    Ok(())
}

/// Adds basic arithmetic words
fn arith(stack: &mut Stack) {
    Vocab::new(stack, "arith")
        .add("+", op_f64_infix!( += ))
        .add("-", op_f64_infix!( -= ))
        .add("*", op_f64_infix!( *= ))
        .add("/", op_f64_infix!( /= ))
        .add("%", op_f64_infix!( %= ))
        .add("neg", op_prefix!(neg));
}

/// Adds comparitive words
fn cmp(stack: &mut Stack) {
    Vocab::new(stack, "cmp")
        .add("max", op_pair!(max))
        .add("min", op_pair!(min))
        .add("=", op_bool!( == ))
        .add(">", op_bool!( > ))
        .add("<", op_bool!( < ))
        .add("!=", op_bool!( != ))
        .add(">=", op_bool!( >= ))
        .add("<=", op_bool!( <= ))
        .add("&&", op_logic!( && ))
        .add("||", op_logic!( || ))
        .add("true", Word::Function(op_true))
        .add("false", Word::Function(op_false))
        .add("not", Word::Function(not));
}

/// Adds words that round and truncate numerics
fn round(stack: &mut Stack) {
    Vocab::new(stack, "round")
        .add("floor", op_prefix!(floor))
        .add("ceil", op_prefix!(ceil))
        .add("round", op_prefix!(round))
        .add("trunc", op_prefix!(trunc))
        .add("fract", op_prefix!(fract))
        .add("abs", op_prefix!(abs));
}

/// Adds logarithmic words
fn log(stack: &mut Stack) {
    Vocab::new(stack, "log")
        .add("exp", op_prefix!(exp))
        .add("exp2", op_prefix!(exp2))
        .add("ln", op_prefix!(ln))
        .add("log", op_pair!(log))
        .add("log2", op_prefix!(log2))
        .add("log10", op_prefix!(log10))
        .add("e", op_const!(E));
}

/// Adds powers words
fn power(stack: &mut Stack) {
    Vocab::new(stack, "powers")
        .add("**", op_pair!(powf))
        .add("sqrt", op_prefix!(sqrt))
        .add("cubert", op_prefix!(cbrt));
}

/// Adds trigonometry words
fn trig(stack: &mut Stack) {
    Vocab::new(stack, "trig")
        .add("sin", op_prefix!(sin))
        .add("cos", op_prefix!(cos))
        .add("tan", op_prefix!(tan))
        .add("asin", op_prefix!(asin))
        .add("acos", op_prefix!(acos))
        .add("atan", op_prefix!(atan))
        .add("sinh", op_prefix!(sinh))
        .add("cosh", op_prefix!(cosh))
        .add("tanh", op_prefix!(tanh))
        .add("asinh", op_prefix!(asinh))
        .add("acosh", op_prefix!(acosh))
        .add("atanh", op_prefix!(atanh))
        .add("pi", op_const!(PI))
        .add("tau", op_const!(TAU));
}

/// Adds core stack manipulations
fn stacks(stack: &mut Stack) {
    Vocab::new(stack, "stack")
        .add("clear", Word::Function(clear))
        .add("drop", Word::Function(drop))
        .add("pick", Word::Function(pick))
        .add("roll", Word::Function(roll))
        .add("dup?", Word::Function(dup_nonzero))
        .add("depth", Word::Function(depth));
}

/// Adds input/output words
fn io(stack: &mut Stack) {
    Vocab::new(stack, "io")
        .add(".", Word::Function(dot))
        .add(".>", Word::Function(dotr))
        .add(".<", Word::Function(dotl))
        .add(".^", Word::Function(dotc))
        .add(",", Word::Function(comma))
        .add("dump", Word::Function(dump))
        .add("emit", Word::Function(emit))
        .add("flush", Word::Function(flush));
}

/// Adds general rrpn system words
fn system(stack: &mut Stack) {
    Vocab::new(stack, "system").add("help", Word::Function(help));
}

/// Adds time related words
fn timing(stack: &mut Stack) {
    Vocab::new(stack, "time")
        .add("sleep", Word::Function(sleep))
        .add("time-now", Word::Function(now))
        .add("time-elapsed", Word::Function(elapsed));
}

/// Adds basic arithmetic words
fn r_arith(stack: &mut Stack) {
    Vocab::new(stack, "rational")
        .add("r+", op_r64_infix!( += ))
        .add("r-", op_r64_infix!( -= ))
        .add("r*", op_r64_infix!( *= ))
        .add("r/", op_r64_infix!( /= ))
        .add("r%", op_r64_infix!( %= ));
}

/// Adds basic arithmetic words
fn c_arith(stack: &mut Stack) {
    Vocab::new(stack, "complex")
        .add("c+", op_c64_infix!( += ))
        .add("c-", op_c64_infix!( -= ))
        .add("c*", op_c64_infix!( *= ))
        .add("c/", op_c64_infix!( /= ))
        .add("c%", op_c64_infix!( %= ))
        .add("c**", op_c64_pair!(powc))
        .add("csin", op_c64_prefix!(sin))
        .add("ccos", op_c64_prefix!(cos))
        .add("ctan", op_c64_prefix!(tan));
}

fn module_rrpnp( stack: &mut Stack ) -> Result {
    let script = include_str!("../scripts/rrpn+");
    for input in script.split( "\n" ) {
        match stack.command(&input) {
            Ok(_) | Err(Error::Comment) => (),
            Err(error) => stack.io.println(&format!( "ERR: {}", error ) ),
        }
    }
    Ok(())
}

fn modules(stack: &mut Stack ) {
    Vocab::new(stack, "modules")
        .add("rrpn+", Word::Function(module_rrpnp));
}

/// Teaches all known vocabs in this file
pub fn teach(stack: &mut Stack) {
    arith(stack);
    cmp(stack);
    round(stack);
    log(stack);
    power(stack);
    trig(stack);
    stacks(stack);
    io(stack);
    system(stack);
    timing(stack);
    r_arith(stack);
    c_arith(stack);
    modules(stack);
}
