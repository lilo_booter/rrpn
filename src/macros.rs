//! Provides macros for defining new words.

/// Basic arithmetic operations
#[macro_export]
macro_rules! op_infix {
	( $op: tt, $entry: ident, $type: ident, $mut_type: ident ) => {
		Word::Function( | stack: &mut Stack | -> Result {
			let tos = stack.pop( ).ok_or( Error::Underflow )?.$type( ).ok_or( Error::Conversion )?;
			let bos = stack.last( ).ok_or( Error::Underflow )?;
			match *bos {
				Entry::$entry( _value ) => { },
				_ => *bos = Entry::$entry( bos.$type().ok_or( Error::Conversion )? ),
			};
			let bos = bos.$mut_type( ).ok_or( Error::Conversion )?;
			*bos $op tos;
			Ok( ( ) )
		} )
	}
}

#[macro_export]
macro_rules! op_f64_infix {
    ( $op: tt ) => {
        op_infix!($op, Double, as_f64, as_mut_f64)
    };
}

#[macro_export]
macro_rules! op_r64_infix {
    ( $op: tt ) => {
        op_infix!($op, Rational, as_r64, as_mut_r64)
    };
}

#[macro_export]
macro_rules! op_c64_infix {
    ( $op: tt ) => {
        op_infix!($op, Complex, as_c64, as_mut_c64)
    };
}

/// Basic boolean comparisons
#[macro_export]
macro_rules! op_bool {
	( $op: tt ) => {
		Word::Function( | stack: &mut Stack | -> Result {
			let tos = stack.pop( ).ok_or( Error::Underflow )?;
			let bos = stack.last( ).ok_or( Error::Underflow )?;
			*bos = Entry::Bool( *bos $op tos );
			Ok( ( ) )
		} )
	}
}

/// Basic logical operations
#[macro_export]
macro_rules! op_logic {
	( $op: tt ) => {
		Word::Function( | stack: &mut Stack | -> Result {
			let tos = stack.pop( ).ok_or( Error::Underflow )?.as_bool( ).ok_or( Error::Conversion )?;
			let bos = stack.last( ).ok_or( Error::Underflow )?.as_mut_bool( ).ok_or( Error::Conversion )?;
			*bos = *bos $op tos;
			Ok( ( ) )
		} )
	}
}

/// Maths single operand operators
#[macro_export]
macro_rules! op_prefix {
    ( $op: ident ) => {
        Word::Function(|stack: &mut Stack| -> Result {
            let tos = stack
                .last()
                .ok_or(Error::Underflow)?
                .as_mut_f64()
                .ok_or(Error::Conversion)?;
            *tos = tos.$op();
            Ok(())
        })
    };
}

#[macro_export]
macro_rules! op_c64_prefix {
    ( $op: ident ) => {
        Word::Function(|stack: &mut Stack| -> Result {
            let tos = stack
                .last()
                .ok_or(Error::Underflow)?
                .as_mut_c64()
                .ok_or(Error::Conversion)?;
            *tos = tos.$op();
            Ok(())
        })
    };
}

/// Maths two operand operators
#[macro_export]
macro_rules! op_pair {
    ( $op: ident ) => {
        Word::Function(|stack: &mut Stack| -> Result {
            let tos = stack
                .pop()
                .ok_or(Error::Underflow)?
                .as_f64()
                .ok_or(Error::Conversion)?;
            let bos = stack
                .last()
                .ok_or(Error::Underflow)?
                .as_mut_f64()
                .ok_or(Error::Conversion)?;
            *bos = bos.$op(tos);
            Ok(())
        })
    };
}

#[macro_export]
macro_rules! op_c64_pair {
    ( $op: ident ) => {
        Word::Function(|stack: &mut Stack| -> Result {
            let tos = stack
                .pop()
                .ok_or(Error::Underflow)?
                .as_c64()
                .ok_or(Error::Conversion)?;
            let bos = stack
                .last()
                .ok_or(Error::Underflow)?
                .as_mut_c64()
                .ok_or(Error::Conversion)?;
            *bos = bos.$op(tos);
            Ok(())
        })
    };
}

/// Math constants
#[macro_export]
macro_rules! op_const {
    ( $op: ident ) => {
        Word::Function(|stack: &mut Stack| -> Result {
            stack.push(Entry::Double(f64::$op()));
            Ok(())
        })
    };
}

/// Generates an error for the specified token
#[macro_export]
macro_rules! op_error {
    ( $err: ident, $op: expr ) => {
        Word::Function(|_stack: &mut Stack| -> Result { Err(Error::$err($op.to_string())) })
    };
}

/// Instantiates a parser
#[macro_export]
macro_rules! op_parser {
    ( $op: tt ) => {
        Word::Parser(|stack: &mut Stack| -> Result {
            stack.parser.push(Rc::new($op::new()));
            Ok(())
        })
    };
}

pub(crate) use op_bool;
pub(crate) use op_const;
pub(crate) use op_error;
pub(crate) use op_f64_infix;
pub(crate) use op_infix;
pub(crate) use op_logic;
pub(crate) use op_pair;
pub(crate) use op_parser;
pub(crate) use op_prefix;
pub(crate) use op_r64_infix;
pub(crate) use op_c64_infix;
pub(crate) use op_c64_pair;
pub(crate) use op_c64_prefix;
