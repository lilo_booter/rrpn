//! Rust implementation of a Reverse Polish Notation calculator.

#![warn(clippy::pedantic)]
#![allow(clippy::unnecessary_wraps)]

mod lexicon;
pub mod macros;
pub mod parsers;
mod split;
pub mod stack;
pub mod vocab;
pub mod io;
