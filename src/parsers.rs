//! Parsers for word definition and conditional/branching flow control

#![warn(clippy::pedantic)]
#![allow(clippy::unnecessary_wraps)]

use num_traits::cast::cast;

use std::rc::Rc;

use crate::stack::Entry;
use crate::stack::Error;
use crate::stack::FullParser;
use crate::stack::Parser;
use crate::stack::Result;
use crate::stack::Stack;
use crate::stack::State;
use crate::stack::Vector;
use crate::stack::Word;

use crate::vocab::Vocab;

use crate::macros::op_error;
use crate::macros::op_parser;

/// Some parsers derive an internal list of tokens and ready to evaluate parser instances
#[derive(Debug)]
enum DefineFn {
    Word(Rc<Word>),
    Parser(Rc<dyn FullParser>),
}

impl DefineFn {
    /// Evaluates a word on the given stack
    pub fn eval(&self, stack: &mut Stack) -> Result {
        match self {
            DefineFn::Word(word) => word.eval(stack)?,
            DefineFn::Parser(parser) => match parser.word() {
                Some(name) => stack.add(&name, Word::Define(parser.clone())),
                None => parser.eval(stack)?,
            },
        };
        Ok(())
    }
}

fn compile(stack: &Stack, token: &str) -> DefineFn {
    match String::from(token).parse::<f64>() {
        Ok(value) => DefineFn::Word(Rc::new(Word::Value(Entry::Double(value)))),
        _ => match stack.get(token) {
            Some(word) => DefineFn::Word(word.clone()),
            _ => DefineFn::Word(Rc::new(Word::Token(token.to_string()))),
        },
    }
}

/// Evaluate a list of tokens and parsers
fn evaluate(stack: &mut Stack, list: &Vec<DefineFn>) -> Result {
    for item in list {
        item.eval(stack)?;
    }
    Ok(())
}

/// Parser for "include \<filename\>"
#[derive(Debug)]
struct ParserInclude {
    filename: Option<String>,
}

/// Implementation of `ParserInclude`
impl ParserInclude {
    /// Constructor of `ParserInclude`
    fn new() -> Self {
        Self { filename: None }
    }
}

/// Implementation of `Parser` for `ParserInclude`
impl Parser for ParserInclude {
    /// The parse method simply consumes the token as a filename and the parser is done
    fn parse(&mut self, _stack: &Stack, token: &str) -> State {
        self.filename = Some(token.to_string());
        State::Done
    }

    /// Include the filename on evaluation
    fn eval(&self, stack: &mut Stack) -> Result {
        stack.include(self.filename.as_ref().ok_or(Error::ParserError)?)
    }
}

impl FullParser for ParserInclude {}

/// Parser for "forget \<word\>"
#[derive(Debug)]
struct ParserForget {
    word: Option<String>,
}

/// Implementation of `ParserForget`
impl ParserForget {
    fn new() -> Self {
        Self { word: None }
    }
}

/// Implementation of Parser for `ParserForget`
impl Parser for ParserForget {
    /// The parse method simply consumes the token as a word and the parser is done
    fn parse(&mut self, _stack: &Stack, token: &str) -> State {
        self.word = Some(token.to_string());
        State::Done
    }

    /// Forget the requested word on evaluation
    fn eval(&self, stack: &mut Stack) -> Result {
        let word = self.word.as_ref().ok_or(Error::ParserError)?;
        if !stack.exists(word) {
            return Err(Error::InvalidToken(word.to_string()));
        }
        stack.forget(word);
        Ok(())
    }
}

impl FullParser for ParserForget {}

/// Parser for "see \<word\>"
#[derive(Debug)]
struct ParserSee {
    word: Option<String>,
}

/// Implementation of `ParserSee`
impl ParserSee {
    fn new() -> Self {
        Self { word: None }
    }
}

/// Implementation of Parser for `ParserSee`
impl Parser for ParserSee {
    /// The parse method simply consumes the token as a word and the parser is done
    fn parse(&mut self, _stack: &Stack, token: &str) -> State {
        self.word = Some(token.to_string());
        State::Done
    }

    /// See the requested word on evaluation
    fn eval(&self, stack: &mut Stack) -> Result {
        let word = self.word.as_ref().ok_or(Error::ParserError)?;
        let def = stack
            .get(word)
            .ok_or_else(|| Error::InvalidToken(word.to_string()))?
            .clone();
        stack.io.println(&format!("{:#?}", def));
        Ok(())
    }
}

impl FullParser for ParserSee {}

/// Parser for "$ \<string\>"
#[derive(Debug)]
struct ParserString {
    string: Option<String>,
}

/// Implementation of `ParserString`
impl ParserString {
    fn new() -> Self {
        Self { string: None }
    }
}

/// Implementation of Parser for `ParserString`
impl Parser for ParserString {
    /// The parse method simply consumes the token as a string and the parser is done
    fn parse(&mut self, _stack: &Stack, token: &str) -> State {
        self.string = Some(token.to_string());
        State::Done
    }

    /// Push copy of string to stack
    fn eval(&self, stack: &mut Stack) -> Result {
        let string = self.string.as_ref().ok_or(Error::ParserError)?;
        stack.push(Entry::String(string.to_string()));
        Ok(())
    }
}

impl FullParser for ParserString {}

/// Parser for "\<value\> assign \<name\>"
#[derive(Debug)]
struct ParserAssign {
    word: Option<String>,
}

/// Implementation of `ParserAssign`
impl ParserAssign {
    /// Constructor of `ParserAssign`
    fn new() -> Self {
        Self { word: None }
    }
}

/// Parser implementation
impl Parser for ParserAssign {
    /// Parser simply retains the first token provided as the name of the new word
    fn parse(&mut self, _stack: &Stack, token: &str) -> State {
        self.word = Some(token.to_string());
        State::Done
    }

    /// Create a new word from the value at the top of stack
    fn eval(&self, stack: &mut Stack) -> Result {
        let word = self.word.as_ref().ok_or(Error::ParserError)?;
        let tos = stack.pop().ok_or(Error::Underflow)?;
        stack.add(word, Word::Value(tos));
        Ok(())
    }
}

impl FullParser for ParserAssign {}

/// Parser for "( .. comments .. )"
#[derive(Debug)]
struct ParserComment {
    comment: Vec<String>,
}

/// Implementation of `ParserComment`
impl ParserComment {
    /// Constructor
    fn new() -> Self {
        Self {
            comment: Vec::new(),
        }
    }
}

/// Implementation of Parser for `ParserComment`
impl Parser for ParserComment {
    /// The parse method accepts all input tokens until the closing ) is seem
    fn parse(&mut self, _stack: &Stack, token: &str) -> State {
        self.comment.push(token.to_string());
        if token == ")" {
            return State::Done;
        }
        State::Next
    }

    /// Eval ignores all tokens provided
    fn eval(&self, _stack: &mut Stack) -> Result {
        Ok(())
    }
}

impl FullParser for ParserComment {}

/// Parser for ": name ... ;" definitions
#[derive(Debug)]
struct ParserDefine {
    name: Option<String>,
    define: Vec<DefineFn>,
}

/// Implementaton of `ParserDefine`
impl ParserDefine {
    /// Constructor
    fn new() -> Self {
        Self {
            name: None,
            define: Vec::new(),
        }
    }
}

/// Implementaton of Parser for `ParserDefine`
impl Parser for ParserDefine {
    /// The parse method stores the first token give as the name of the new word, and the
    /// remainder are evaluated as parsers if need be or stored as tokens
    fn parse(&mut self, stack: &Stack, token: &str) -> State {
        // The name must be provided before we're done or we eval
        if Some(self.name.as_ref()) != None {
            // If the current token is ; we're done
            if token == ";" {
                return State::Done;
            }

            // Parsers following the name are always evaluated
            if stack.is_parser(token) {
                return State::Eval;
            }
        }

        // Append token to definition
        match self.name {
            None => self.name = Some(token.to_string()),
            _ => self.define.push(compile(stack, token)),
        };

        // Request the next token
        State::Next
    }

    /// We need to save the ready to be evaluated parsers when given
    fn include(&mut self, other: Rc<dyn FullParser>) {
        self.define.push(DefineFn::Parser(other));
    }

    /// Obtains the name of the token being created
    fn word(&self) -> Option<String> {
        self.name.clone()
    }

    /// Evaluate creates the requested word
    fn eval(&self, stack: &mut Stack) -> Result {
        // TODO: Validate name before adding
        stack.local_open( );
        let result = evaluate(stack, &self.define);
        stack.local_close( );
        result
    }
}

impl FullParser for ParserDefine {}

/// Parser for "<value> ?{ .. non-zero ... }:{ .... zero ... }" conditional branching - also supports "?{ ... non-zero ... }"
#[derive(Debug)]
struct ParserCondition {
    branch: bool,
    if_true: Vec<DefineFn>,
    if_false: Vec<DefineFn>,
}

/// Implementation of `ParserCondition`
impl ParserCondition {
    /// Constructor
    fn new() -> Self {
        Self {
            branch: true,
            if_true: Vec::new(),
            if_false: Vec::new(),
        }
    }
}

/// Implementation of `Parser` for `ParserCondition`
impl Parser for ParserCondition {
    /// The conditional's parse always requires nested parsers to be expanded - all other tokens
    /// are added to either the `if_true` or `if_false` vectors
    fn parse(&mut self, stack: &Stack, token: &str) -> State {
        if stack.is_parser(token) {
            return State::Eval;
        }

        if token == "}" {
            return State::Done;
        }

        if token != "}:{" || !self.branch {
            let branch = if self.branch {
                &mut self.if_true
            } else {
                &mut self.if_false
            };
            branch.push(compile(stack, token));
        } else {
            self.branch = false;
        }

        // Request the next token
        State::Next
    }

    /// Push parsers in the active branch of the conditional
    fn include(&mut self, other: Rc<dyn FullParser>) {
        if self.branch {
            &mut self.if_true
        } else {
            &mut self.if_false
        }
        .push(DefineFn::Parser(other));
    }

    /// Evaluate the conditional
    fn eval(&self, stack: &mut Stack) -> Result {
        let top = stack
            .pop()
            .ok_or(Error::Underflow)?
            .as_bool()
            .ok_or(Error::Conversion)?;
        let list = if top { &self.if_true } else { &self.if_false };
        evaluate(stack, list)
    }
}

impl FullParser for ParserCondition {}

/// Parser for ":: \<vocab\> .... ;" definitions
#[derive(Debug)]
struct ParserVocab {
    name: Option<String>,
    define: Vec<DefineFn>,
}

/// Implementation of `ParserVocab`
impl ParserVocab {
    /// Constructor
    fn new() -> Self {
        Self {
            name: None,
            define: Vec::new(),
        }
    }
}

/// Implementation of `Parser` for `ParserVocab`
impl Parser for ParserVocab {
    /// The first token the parse method receives is the the vocab's name - all parsers are
    /// expanded and included as required
    fn parse(&mut self, stack: &Stack, token: &str) -> State {
        // The name must be provided before we're done or we eval
        if Some(self.name.as_ref()) != None {
            // If the current token is ; we're done
            if token == ";" {
                return State::Done;
            }

            // Parsers following the name are always evaluated
            if stack.is_parser(token) {
                return State::Eval;
            }
        }

        // Append token to definition
        match self.name {
            None => self.name = Some(token.to_string()),
            _ => self.define.push(compile(stack, token)),
        };

        // Request the next token
        State::Next
    }

    /// Appends expanded parsers
    fn include(&mut self, other: Rc<dyn FullParser>) {
        self.define.push(DefineFn::Parser(other));
    }

    /// Evaluates the lists of tokens and parsers collected
    fn eval(&self, stack: &mut Stack) -> Result {
        // TODO: Validate name before adding
        let vocab = Vocab::new(stack, self.name.as_ref().unwrap());
        evaluate(vocab.stack, &self.define)
    }
}

impl FullParser for ParserVocab {}

/// Index guard
pub struct Index<'a> {
    pub stack: &'a mut Stack,
    pub index: usize,
}

/// Implementation of index guard
impl<'a> Index<'a> {
    /// Constructor for index guard
    pub fn new(stack: &'a mut Stack, value: Entry) -> Self {
        let index = stack.indexes.len();
        stack.indexes.push(value);
        Self { stack, index }
    }

    fn set(&mut self, entry: Entry) {
        *(*self.stack).indexes.get_mut(self.index).unwrap() = entry;
    }
}

impl<'a> Drop for Index<'a> {
    /// Destructor for index guard
    fn drop(&mut self) {
        (*self.stack).indexes.pop();
    }
}

/// Parser for "n1 n2 do ... loop" definitions
#[derive(Debug)]
struct ParserDo {
    define: Vec<DefineFn>,
    plus: bool,
}

/// Implementaton of `ParserDo`
impl ParserDo {
    /// Constructor
    fn new() -> Self {
        Self {
            define: Vec::new(),
            plus: false,
        }
    }
}

/// Implementaton of Parser for `ParserDo`
impl Parser for ParserDo {
    /// The parse method stores the first token give as the name of the new word, and the
    /// remainder are evaluated as parsers if need be or stored as tokens
    fn parse(&mut self, stack: &Stack, token: &str) -> State {
        // If the current token is loop we're done
        if token == "loop" {
            return State::Done;
        }
        if token == "+loop" {
            self.plus = true;
            return State::Done;
        }

        // Parsers are always evaluated
        if stack.is_parser(token) {
            return State::Eval;
        }

        // Append token to definition
        self.define.push(compile(stack, token));

        // Request the next token
        State::Next
    }

    /// We need to save the ready to be evaluated parsers when given
    fn include(&mut self, other: Rc<dyn FullParser>) {
        self.define.push(DefineFn::Parser(other));
    }

    /// Evaluate the loop
    fn eval(&self, stack: &mut Stack) -> Result {
        let mut start = stack
            .pop()
            .ok_or(Error::Underflow)?
            .as_f64()
            .ok_or(Error::Conversion)?;
        let end = stack
            .pop()
            .ok_or(Error::Underflow)?
            .as_f64()
            .ok_or(Error::Conversion)?;

        let mut index = Index::new(stack, Entry::Double(start));

        while start < end {
            // Evaluate until done, error or leave
            match evaluate(index.stack, &self.define) {
                Ok(()) => (),
                Err(Error::Leave(_)) => break,
                err => return err,
            };

            // Honour the loop or +loop request
            let increment = if self.plus {
                index
                    .stack
                    .pop()
                    .ok_or(Error::Underflow)?
                    .as_f64()
                    .ok_or(Error::Conversion)?
            } else {
                1.0
            };
            start += increment;
            index.set(Entry::Double(start));
        }

        Ok(())
    }
}

impl FullParser for ParserDo {}

fn get_index(stack: &mut Stack, index: usize) -> Result {
    if index > stack.indexes.len() {
        return Err(Error::IndexNotInScope(index.to_string()));
    }
    let value = stack
        .indexes
        .get(stack.indexes.len() - index - 1)
        .ok_or_else(|| Error::IndexNotInScope(index.to_string()))?
        .clone();
    stack.push(value);
    Ok(())
}

fn index(stack: &mut Stack) -> Result {
    let index = stack
        .pop()
        .ok_or(Error::Underflow)?
        .as_f64()
        .ok_or(Error::Conversion)?;
    let index: usize = cast(index).unwrap();
    get_index(stack, index)
}

fn i(stack: &mut Stack) -> Result {
    get_index(stack, 0)
}

fn j(stack: &mut Stack) -> Result {
    get_index(stack, 1)
}

fn k(stack: &mut Stack) -> Result {
    get_index(stack, 2)
}

/// Parser for "p, \<string\>"
#[derive(Debug)]
struct ParserPrint {
    content: Option<String>,
}

/// Implementation of `ParserPrint`
impl ParserPrint {
    /// Constructor of `ParserPrint`
    fn new() -> Self {
        Self { content: None }
    }
}

/// Implementation of `Parser` for `ParserPrint`
impl Parser for ParserPrint {
    /// The parse method simply consumes the token as the content and the parser is done
    fn parse(&mut self, _stack: &Stack, token: &str) -> State {
        self.content = Some(token.to_string());
        State::Done
    }

    /// Include the filename on evaluation
    fn eval(&self, stack: &mut Stack) -> Result {
        stack.io.print(&format!("{}", self.content.as_ref().unwrap()));
        Ok(())
    }
}

impl FullParser for ParserPrint {}

fn eval(stack: &mut Stack) -> Result {
    let entry = stack.pop().ok_or(Error::Underflow)?;
    if let Entry::String(value) = entry {
        stack.command(&*value)
    } else {
        stack.push(entry);
        Ok(())
    }
}

/// Parser for "[ entry .. ]"
#[derive(Debug)]
struct ParserVector {
    vector: Vector,
}

/// Implementation of `ParserVector`
impl ParserVector {
    /// Constructor
    fn new() -> Self {
        Self {
            vector: Vector::new(),
        }
    }
}

/// Implementation of Parser for `ParserVector`
impl Parser for ParserVector {
    /// The parse method accepts all input tokens until the closing ] is seem
    fn parse(&mut self, _stack: &Stack, token: &str) -> State {
        if token == "[" {
            return State::Eval;
        }
        if token == "]" {
            return State::Done;
        }
        self.vector.data.push(Entry::eval(token));
        State::Next
    }

    fn include(&mut self, other: Rc<dyn FullParser>) {
        self.vector.data.push(other.entry().unwrap());
    }

    fn entry(&self) -> Option<Entry> {
        Some(Entry::Vector(self.vector.clone()))
    }

    fn eval(&self, stack: &mut Stack) -> Result {
        stack.push(Entry::Vector(self.vector.clone()));
        Ok(())
    }
}

impl FullParser for ParserVector {}

/// Parser for "\<vector\> for ... each" definitions
#[derive(Debug)]
struct ParserFor {
    define: Vec<DefineFn>,
}

/// Implementaton of `ParserFor`
impl ParserFor {
    /// Constructor
    fn new() -> Self {
        Self { define: Vec::new() }
    }
}

/// Implementaton of Parser for `ParserFor`
impl Parser for ParserFor {
    /// The parse method stores the first token give as the name of the new word, and the
    /// remainder are evaluated as parsers if need be or stored as tokens
    fn parse(&mut self, stack: &Stack, token: &str) -> State {
        // If the current token is loop we're done
        if token == "each" {
            return State::Done;
        }

        // Parsers are always evaluated
        if stack.is_parser(token) {
            return State::Eval;
        }

        // Append token to definition
        self.define.push(compile(stack, token));

        // Request the next token
        State::Next
    }

    /// We need to save the ready to be evaluated parsers when given
    fn include(&mut self, other: Rc<dyn FullParser>) {
        self.define.push(DefineFn::Parser(other));
    }

    /// Evaluate the loop
    fn eval(&self, stack: &mut Stack) -> Result {
        let tos = stack.pop().ok_or(Error::Underflow)?;

        let list = match tos {
            Entry::Vector(value) => value,
            _ => Vector::new(),
        };

        let mut index = Index::new(stack, Entry::Double(0.0));

        for item in &list.data {
            index.set(item.clone());

            // Evaluate until done, error or leave
            match evaluate(index.stack, &self.define) {
                Ok(()) => (),
                Err(Error::Leave(_)) => break,
                err => return err,
            };
        }

        Ok(())
    }
}

impl FullParser for ParserFor {}

fn v_len(stack: &mut Stack) -> Result {
    let entry = stack.data.last().ok_or(Error::Underflow)?;
    match entry {
        Entry::Vector(value) => {
            let v = cast(value.data.len()).unwrap();
            stack.push(Entry::Double(v));
            Ok(())
        }
        _ => Err(Error::InvalidType("v#".to_string())),
    }
}

fn v_get(stack: &mut Stack) -> Result {
    let index = stack.data.pop().ok_or(Error::Underflow)?;
    let index = index.as_usize();
    let index = index.ok_or_else(|| Error::InvalidType("v@".to_string()))?;
    let entry = stack.data.last().ok_or(Error::Underflow)?;
    match entry {
        Entry::Vector(vec) => {
            if index >= vec.data.len() {
                return Err(Error::OutOfBounds("v@".to_string()));
            }
            let item = vec.data[index].clone();
            stack.push(item);
        }
        _ => {
            return Err(Error::InvalidType("v@".to_string()));
        }
    };
    Ok(())
}

fn v_set(stack: &mut Stack) -> Result {
    let index = stack.data.pop().ok_or(Error::Underflow)?;
    let index = index.as_usize();
    let index = index.ok_or_else(|| Error::InvalidType("v!".to_string()))?;
    let value = stack.data.pop().ok_or(Error::Underflow)?;
    let entry = stack.last().ok_or(Error::Underflow)?;
    match entry {
        Entry::Vector(vec) => {
            if index >= vec.data.len() {
                return Err(Error::OutOfBounds("v!".to_string()));
            }
            let item = &mut vec.data[index];
            *item = value;
        }
        _ => {
            return Err(Error::InvalidType("v!".to_string()));
        }
    };
    Ok(())
}

fn v_add(stack: &mut Stack) -> Result {
    let value = stack.data.pop().ok_or(Error::Underflow)?;
    let entry = stack.last().ok_or(Error::Underflow)?;
    match entry {
        Entry::Vector(vec) => {
            vec.data.push(value);
        }
        _ => {
            return Err(Error::InvalidType("v+".to_string()));
        }
    };
    Ok(())
}

fn v_see(stack: &mut Stack) -> Result {
    let entry = stack.last().ok_or(Error::Underflow)?;
    match entry {
        Entry::Vector(_) => {
            let out = format!("{}", entry);
            stack.io.println(&out);
        }
        _ => {
            return Err(Error::InvalidType("v+".to_string()));
        }
    };
    Ok(())
}

fn v_remove(stack: &mut Stack) -> Result {
    let index = stack.data.pop().ok_or(Error::Underflow)?;
    let index = index.as_usize();
    let index = index.ok_or_else(|| Error::InvalidType("v-".to_string()))?;
    let entry = stack.last().ok_or(Error::Underflow)?;
    match entry {
        Entry::Vector(vec) => {
            if index >= vec.data.len() {
                return Err(Error::OutOfBounds("v-".to_string()));
            }
            vec.data.remove(index);
        }
        _ => {
            return Err(Error::InvalidType("v-".to_string()));
        }
    };

    Ok(())
}

fn v_insert(stack: &mut Stack) -> Result {
    let index = stack.data.pop().ok_or(Error::Underflow)?;
    let index = index.as_usize();
    let index = index.ok_or_else(|| Error::InvalidType("v^".to_string()))?;
    let value = stack.data.pop().ok_or(Error::Underflow)?;
    let entry = stack.last().ok_or(Error::Underflow)?;
    match entry {
        Entry::Vector(vec) => {
            if index > vec.data.len() {
                return Err(Error::OutOfBounds("v^".to_string()));
            }
            vec.data.insert(index, value);
        }
        _ => {
            return Err(Error::InvalidType("v^".to_string()));
        }
    };

    Ok(())
}

/// Teaches stack about word/vocab creation methods
fn words(stack: &mut Stack) {
    Vocab::new(stack, "words")
        .add("include", op_parser!(ParserInclude))
        .add("forget", op_parser!(ParserForget))
        .add("see", op_parser!(ParserSee))
        .add("assign", op_parser!(ParserAssign))
        .add("(", op_parser!(ParserComment))
        .add(")", op_error!(CompilerWord, ")"))
        .add(":", op_parser!(ParserDefine))
        .add("::", op_parser!(ParserVocab))
        .add(";", op_error!(CompilerWord, ";"));
}

/// Teaches stack about conditional functionality
fn conditionals(stack: &mut Stack) {
    Vocab::new(stack, "branch")
        .add("?{", op_parser!(ParserCondition))
        .add("}:{", op_error!(CompilerWord, "}:{"))
        .add("}", op_error!(CompilerWord, "}"));
}

fn loops(stack: &mut Stack) {
    Vocab::new(stack, "loops")
        .add("leave", op_error!(Leave, "leave"))
        .add("do", op_parser!(ParserDo))
        .add("loop", op_error!(CompilerWord, "loop"))
        .add("+loop", op_error!(CompilerWord, "+loop"))
        .add("index?", Word::Function(index))
        .add("i", Word::Function(i))
        .add("j", Word::Function(j))
        .add("k", Word::Function(k));
}

fn io(stack: &mut Stack) {
    Vocab::new(stack, "io").add("p,", op_parser!(ParserPrint));
}

fn strings(stack: &mut Stack) {
    Vocab::new(stack, "strings")
        .add("$", op_parser!(ParserString))
        .add("eval", Word::Function(eval));
}

fn vectors(stack: &mut Stack) {
    Vocab::new(stack, "vectors")
        .add("[", op_parser!(ParserVector))
        .add("]", op_error!(CompilerWord, "]"))
        .add("for", op_parser!(ParserFor))
        .add("each", op_error!(CompilerWord, "each"))
        .add("v#", Word::Function(v_len))
        .add("v@", Word::Function(v_get))
        .add("v!", Word::Function(v_set))
        .add("v+", Word::Function(v_add))
        .add("v?", Word::Function(v_see))
        .add("v-", Word::Function(v_remove))
        .add("v^", Word::Function(v_insert));
}

/// Teaches stack about known parsers
pub fn teach(stack: &mut Stack) {
    words(stack);
    conditionals(stack);
    loops(stack);
    io(stack);
    strings(stack);
    vectors(stack);
}
