//! Traits and default imeplmentation for an Io struct

use std::io::stdout;
use std::io::Write;

pub trait Io {
    fn println( &self, s: &str );
    fn print( &self, s: &str );
	fn flush( &self );
}

pub struct Stdio { }

impl Stdio {
    pub fn new( ) -> Self {
        Stdio { }
    }
}

impl Io for Stdio {
    fn println( &self, s: &str ) {
        println!( "{}", s );
    }

    fn print( &self, s: &str ) {
        print!( "{}", s );
    }

	fn flush( &self ) {
		let _ignore = stdout().flush();
	}
}
