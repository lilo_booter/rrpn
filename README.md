# rrpn

Rust implementation of an [RPN] calculator written as an experimental toy in order
to learn the Rust programming language and build environment.

It can be used as a command line tool or as an interactive shell with command line
history, tab completion, variables and user defined functions. It also support
conditional branching, loops and recursion.

It supports:

* floating point numbers
* rationals
* complex numbers
* booleans
* strings
* vectors of any support type

The usage of the tool is:

```
rrpn [ file ] [ token ... ] *
```

When file exists, the contents of it are evaulated. If additional arguments are
provided, these are evaulated and the process with terminate, otherwise a shell
is provided.

To access the shell, run rrpn without arguments:

```
$ rrpn
> : deg2rad pi * 180 / ;
> : rad2deg 180 * pi / ;
> 360 deg2rad dump rad2deg dump
# 6.28318530717959
# 360.0
> assign degrees
> assign radians
> degrees .
360.0
> radians .
6.28318530717959
```

An example of a script file is provided in the scripts directory, and assuming rrpn
is installed and on your PATH, in bash and similar shells which use shebangs, this
can be used like:

```
$ ./scripts/rrpn+
> help
angles      : degree-to-radian degree-to-turn radian-to-degree radian-to-turn turn-to-degres turn-to-radian
area        : acre-to-hectare acre-to-square-km hectare-to-acre square-km-to-acre square-km-to-square-mile square-mile-to-square-km
arith       : % * + - / inv neg
cmp         : != && < <= = > >= max min not ||
conditions  : ?{ } }:{
distance    : cm-to-feet cm-to-inch inch-to-cm km-to-metre km-to-mile mile-to-km
io          : . dump emit flush
log         : e exp exp2 ln log log10 log2
loops       : +loop do i index? j k leave loop
powers      : ** cube cubert sqrt square
round       : abs ceil floor fract round trunc
rust        : mul_add signum
stack       : -rot 2drop 2dup 2nip 2over 2rot 2swap 2tuck clear depth drop dup dup? nip over pick roll rot swap tuck
system      : help history
temperature : celsius-to-faranheit celsius-to-kelvin faranheit-to-celsius faranheit-to-kelvin kelvin-to-celsius kelvin-to-faranheit
trig        : acos acosh asin asinh atan atanh cos cosh pi sin sinh tan tanh tau
volume      : imperial-gallon-to-litre litre-to-imperial-gallon litre-to-us-gallon us-gallon-to-litre
weights     : g-to-ounces kg-to-lb kg-to-stones lb-to-kg lb-to-ounces ounces-to-g ounces-to-lb stone-to-kg
words       : ( ) : :: ; assign forget include
>
```

Tab completion is also provided:

```
$ rrpn
> h[tab][tab]
help     history  hypot
> he[tab]
> help
etc
```

When arguments are specified, they are evaluated and the process terminates:

```
$ rppn help
arith      : % * + - / neg
cmp        : != && < <= = > >= max min not ||
conditions : ?{ } }:{
io         : . dump emit flush
log        : e exp exp2 ln log log10 log2
loops      : +loop do i index? j k leave loop
powers     : ** cubert sqrt
round      : abs ceil floor fract round trunc
rust       : mul_add signum
stack      : clear depth drop dup? pick roll
system     : help history
trig       : acos acosh asin asinh atan atanh cos cosh pi sin sinh tan tanh tau
words      : ( ) : :: ; assign forget include
```

The top of stack is automatically displayed on exit and it can be used as a
scripting component in your shell:

```
$ rrpn 1 2 +
3.0
$ rrpn 1 2 3 "*" +
7.0
$ val=$( rrpn 1 2 3 "*" + )
$ echo $val
7.0
```

Note that * is typically globbed by your shell and will be replaced by the files in
the current directory - hence they should be quoted or escaped according to the
rules of your shell.

## Build

To build the source code locally:

* clone the git repo
* install rust by way of [rustup]

and build using the command:

```
cargo install --example rrpn --path .
```

## Vocabularies

`rrpn` organises its dictionary in vocabularies. The following shows the contents and
use of each 'word' within each 'vocab'.

### `system`

Provides general info about the `rrpn` system.

| Word      | Usage     | Result | Comment                           |
| --------- | --------- | ------ | --------------------------------- |
| `help`    | `help`    |        | Displays the stack's dictionary   |
| `history` | `history` |        | Displays the command line history |

### `arith`

Provides basic arithmetic.

| Word   | Usage                      | Result     | Comment    |
| ------ | -------------------------- | ---------- | ---------- |
| `+`    | `<number> <number> +`      | `<number>` | Adds       |
| `-`    | `<number> <number> -`      | `<number>` | Subtracts  |
| `*`    | `<number> <number> *`      | `<number>` | Multiplies |
| `/`    | `<number> <number> /`      | `<number>` | Divides    |
| `%`    | `<number> <number> %`      | `<number>` | Modulus    |

### `io`

Provides a means to print the top of stack.

| Word     | Usage                   | Result     | Comment                                         |
| -------- | ----------------------- | ---------- | ----------------------------------------------- |
| `.`      | `<item> .`              |            | Prints top item with new line and removes it    |
| `emit`   | `<char> emit`           |            | Prints TOS as a char and removes it             |
| `read`   | `read`                  | `<string>` | Reads a string from stdin and puts it at TOS    |
| `eof`    | `eof`                   | `<bool>`   | Indicates if the input is at eof                |
| `cr`     | `cr`                    |            | Outputs a new line                              |
| `space`  | `space`                 |            | Outputs a space                                 |
| `flush`  | `flush`                 |            | Flushes the output stream                       |
| `prompt` | `<string> prompt`       | `<string>` | Prints tos, reads input and returns result      |

### `string`

Provides a means to introduce a string to the stack.

| Word   | Usage                   | Result      | Comment                                          |
| ------ | ----------------------- | ----------- | ------------------------------------------------ |
| `$`    | `$ <string>`            | `<string>`  | Puts the string at TOS                           |
| `eval` | `<string> eval`         | `???`       | Evaluates the string at TOS                      |

### `stack`

| Word    | Usage                             | Result                                  | Comment                                |
| ------- | --------------------------------- | --------------------------------------- | -------------------------------------- |
| `pick`  | `<int> pick`                      | `<item>`                                | Duplicates the nth item as TOS         |
| `roll`  | `<int> roll`                      | `<item>`                                | Moves the nth item to TOS              |
| `depth` | `depth`                           | `<int>`                                 | Number of items on stack placed as TOS |
| `drop`  | `<item> drop`                     |                                         | Removes the item at TOS                |

[RPN]: https://en.wikipedia.org/wiki/Reverse_Polish_notation
[rustup]: https://www.rust-lang.org/tools/install
